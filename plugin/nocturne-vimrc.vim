set nu
syntax on

nnoremap <silent> tl :tabnext<CR>
nnoremap <silent> th :tabprev<CR>

nnoremap <silent> <C-k> :wincmd k<CR>
nnoremap <silent> <C-j> :wincmd j<CR>
nnoremap <silent> <C-h> :wincmd h<CR>
nnoremap <silent> <C-l> :wincmd l<CR>

set expandtab
set tabstop=2
set shiftwidth=2

let g:jellybeans_use_lowcolor_black=0
colors jellybeans
